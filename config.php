<?php
/**                                                                             
* Disable deprecation warnings.                                                
*/                                                                             
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

/**
 * Database credentials.
 */
$dbhost = "localhost";
$dbdatabase = "cineforums";
$dbuser = "Ciprian";
$dbpassword = "passw";

/**
 * Website configuration.
 */
$config_forumsname = "CineForums";
$config_admin = "Ciprian Hanga";
$config_adminemail = "&#099;&#105;&#112;&#114;&#105;&#097;&#110;&#104;&#097;&#110;&#103;&#097;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;";
$config_basedir = "http://sandbox.dev:8080/cineforums/";
